//
//  main.m
//  WaterWorkz
//
//  Created by Rosie on 5/07/2015.
//  Copyright (c) 2015 FreshEdit Pty Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
