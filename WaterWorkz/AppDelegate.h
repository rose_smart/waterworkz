//
//  AppDelegate.h
//  WaterWorkz
//
//  Created by Rosie on 5/07/2015.
//  Copyright (c) 2015 FreshEdit Pty Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

