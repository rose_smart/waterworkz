//
//  LandSelectorViewController.m
//  WaterWorkz
//
//  Created by Rosie on 5/07/2015.
//  Copyright (c) 2015 FreshEdit Pty Ltd. All rights reserved.
//

#import "LandSelectorViewController.h"
#import <MapKit/MapKit.h>
#import "KMLParser.h"


@interface LandSelectorViewController ()
@property (weak, nonatomic) IBOutlet MKMapView *mapView;

@end




@implementation LandSelectorViewController

KMLParser *kmlParser;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.navigationController.navigationBar setBarTintColor:[UIColor whiteColor]];
    UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0, self.navigationController.navigationBar.frame.size.height , self.navigationController.navigationBar.frame.size.width, 1)];
    bgView.backgroundColor = [UIColor whiteColor];
    [self.navigationController.navigationBar addSubview:bgView];
    [self.navigationController.navigationBar setShadowImage:nil];
        UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, 30)];
        [title setTextAlignment:NSTextAlignmentCenter];
        [title setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:20]];
        [title setTextColor:[UIColor grayColor]];
        title.text = @"Seach Melbourne Water Land";
        [self.navigationItem setTitleView:title];
    
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"Liveability_Landuse" ofType:@"kml"];
    NSURL *url = [NSURL fileURLWithPath:path];
    kmlParser = [[KMLParser alloc] initWithURL:url];
    [kmlParser parseKML];
    
    // Add all of the MKOverlay objects parsed from the KML file to the map.
    NSArray *overlays = [kmlParser overlays];
    [self.mapView addOverlays:overlays];
    
    NSObject *o = [self.mapView.overlays objectAtIndex:0];
    /*for (MKPolygon *p in self.mapView.overlays) {
        MKPolygonView *pv = (MKPolygonView *)[self.mapView viewForOverlay:p];
        pv.fillColor = [UIColor grayColor];
        pv.alpha = 0.5;
        [self.mapView bringSubviewToFront:pv];
    }*/
    
    // Add all of the MKAnnotation objects parsed from the KML file to the map.
    NSArray *annotations = [kmlParser points];
    [self.mapView addAnnotations:annotations];
    
    // Walk the list of overlays and annotations and create a MKMapRect that
    // bounds all of them and store it into flyTo.
    MKMapRect flyTo = MKMapRectNull;
    for (id <MKOverlay> overlay in overlays) {
        if (MKMapRectIsNull(flyTo)) {
            flyTo = [overlay boundingMapRect];
        } else {
            flyTo = MKMapRectUnion(flyTo, [overlay boundingMapRect]);
        }
    }
    
    for (id <MKAnnotation> annotation in annotations) {
        MKMapPoint annotationPoint = MKMapPointForCoordinate(annotation.coordinate);
        MKMapRect pointRect = MKMapRectMake(annotationPoint.x, annotationPoint.y, 0, 0);
        if (MKMapRectIsNull(flyTo)) {
            flyTo = pointRect;
        } else {
            flyTo = MKMapRectUnion(flyTo, pointRect);
        }
    }
    
    // Position the map so that all overlays and annotations are visible on screen.
    self.mapView.visibleMapRect = flyTo;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark MKMapViewDelegate

/*- (MKOverlayView *)mapView:(MKMapView *)mapView viewForOverlay:(id <MKOverlay>)overlay
{
    return [kmlParser viewForOverlay:overlay];
}*/

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation
{
    return [kmlParser viewForAnnotation:annotation];
}

- (MKOverlayView *)mapView:(MKMapView *)mapView viewForOverlay:(id <MKOverlay>)overlay {
    if ([overlay isKindOfClass:[MKPolygon class]]) {
        MKPolygonView*    aView = [[MKPolygonView alloc] initWithPolygon:(MKPolygon*)overlay];
        
        aView.fillColor = [[UIColor cyanColor] colorWithAlphaComponent:0.2];
        aView.strokeColor = [UIColor yellowColor];
        aView.lineWidth = 3;
        
        return aView;
    }
    return nil;
}

@end
