//
//  SecondViewController.m
//  WaterWorkz
//
//  Created by Rosie on 5/07/2015.
//  Copyright (c) 2015 FreshEdit Pty Ltd. All rights reserved.
//

#import "SecondViewController.h"
#import <MapKit/MapKit.h>

@interface SecondViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *sitePicImageView;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;

@end

@implementation SecondViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.mapView.layer.borderColor = [[UIColor darkGrayColor] CGColor];
    self.mapView.layer.borderWidth = 1.0f;
    self.sitePicImageView.layer.borderColor = [[UIColor darkGrayColor] CGColor];
    self.sitePicImageView.layer.borderWidth = 1.0f;
    
    //UIView *outerView = [UIView alloc] initWithFrame:CGRectMake(self.mapView.frame.origin.x - 1, self.mapView.frame.origin.y - 1, <#CGFloat width#>, <#CGFloat height#>)
    
    
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
